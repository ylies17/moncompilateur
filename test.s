			# This code was produced by the CERI Compiler
.data
FormatString1:	.string "%llu\n"	# used by printf to display 64-bit unsigned integers
TrueString:	.string "TRUE\n"	# used by printf to display the boolean value TRUE
FalseString:	.string "FALSE\n"	# used by printf to display the boolean value FALSE
a:	.quad 0
b:	.quad 0
c:	.quad 0
	.align 8
	.text		# The following lines contain the program
	.globl main	# The main function must be visible from outside
main:			# The main function body :
	movq %rsp, %rbp	# Save the position of the stack's top
	push $1
	pop a
	push $6
	pop b
While0:
	push a
	push $12
	pop %rax
	pop %rbx
	cmpq %rax, %rbx
	jb Vrai2	# If below
	push $0		# False
	jmp Suite2
Vrai2:	push $0xFFFFFFFFFFFFFFFF		# True
Suite2:
	pop %rax	# Get the result of expression
	cmpq $0, %rax
	je EndWhile0	# if FALSE, jump out of the loop0
	push a
	push $2
	pop %rbx
	pop %rax
	movq $0, %rdx
	div %rbx
	push %rdx	# MOD
	push $0
	pop %rax
	pop %rbx
	cmpq %rax, %rbx
	je Vrai4	# If equal
	push $0		# False
	jmp Suite4
Vrai4:	push $0xFFFFFFFFFFFFFFFF		# True
Suite4:
	pop %rax	# Get the result of expression
	cmpq $0, %rax
	je Else2	# if FALSE, jump to Else2
	push a
	pop %rdx	# The value to be displayed
	movq $FormatString1, %rsi	# "%llu\n"
	movl	$1, %edi
	movl	$0, %eax
	call	__printf_chk@PLT
	jmp Next2	# Do not execute the else statement
Else2:
	push a
	push $2
	pop %rbx
	pop %rax
	mulq	%rbx
	push %rax	# MUL
	pop %rdx	# The value to be displayed
	movq $FormatString1, %rsi	# "%llu\n"
	movl	$1, %edi
	movl	$0, %eax
	call	__printf_chk@PLT
Next2:
	push a
	push $1
	pop %rbx
	pop %rax
	addq	%rbx, %rax	# ADD
	push %rax
	pop a
	push a
	push b
	pop %rax
	pop %rbx
	cmpq %rax, %rbx
	jbe Vrai7	# If below or equal
	push $0		# False
	jmp Suite7
Vrai7:	push $0xFFFFFFFFFFFFFFFF		# True
Suite7:
	pop c
	push c
	pop %rdx	# The value to be displayed
	movq $FormatString1, %rsi	# "%llu\n"
	movl	$1, %edi
	movl	$0, %eax
	call	__printf_chk@PLT
	jmp While0
EndWhile0:
	movq %rbp, %rsp		# Restore the position of the stack's top
	ret			# Return from main function
