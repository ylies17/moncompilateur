			# This code was produced by the CERI Compiler
.data
FormatString1:	.string "%llu"	# used by printf to display 64-bit unsigned integers
FormatString2:	.string "%lf"	# used by printf to display 64-bit floating point numbers
FormatString3:	.string "%c"	# used by printf to display a 8-bit single character
TrueString:	.string "TRUE"	# used by printf to display the boolean value TRUE
FalseString:	.string "FALSE"	# used by printf to display the boolean value FALSE
a:	.quad 0
b:	.quad 0
	.align 8
	.text		# The following lines contain the program
	.globl main	# The main function must be visible from outside
main:			# The main function body :
	movq %rsp, %rbp	# Save the position of the stack's top
	push $4
	pop a
	push $0
	pop b
Case_Start0:
	push a
	pop %rax
Case1:
	push $3
	cmpq (%rsp), %rax	# See if equal
	jne Suite1	# Jump to CaseEnd to skip other cases
	push $6
	pop b
	jmp CaseEnd0	# Jump to CaseEnd to skip other cases
Suite1:
Case2:
	push $4
	cmpq (%rsp), %rax	# See if equal
	jne Suite2	# Jump to CaseEnd to skip other cases
	push $8
	pop b
	jmp CaseEnd0	# Jump to CaseEnd to skip other cases
Suite2:
Case3:
	push $5
	cmpq (%rsp), %rax	# See if equal
	jne Suite3	# Jump to CaseEnd to skip other cases
	push $10
	pop b
	jmp CaseEnd0	# Jump to CaseEnd to skip other cases
Suite3:
CaseEnd0:
	movq $0, %rax
	movb $'a',%al
	push %rax	# push a 64-bit version of 'a'
	pop %rsi			# get character in the 8 lowest bits of %si
	movq $FormatString3, %rdi	# "%c\n"
	movl	$0, %eax
	call	printf@PLT
	movq $0, %rax
	movb $':',%al
	push %rax	# push a 64-bit version of ':'
	pop %rsi			# get character in the 8 lowest bits of %si
	movq $FormatString3, %rdi	# "%c\n"
	movl	$0, %eax
	call	printf@PLT
	push a
	pop %rsi	# The value to be displayed
	movq $FormatString1, %rdi	# "%llu\n"
	movl	$0, %eax
	call	printf@PLT
	movq $0, %rax
	movb $',',%al
	push %rax	# push a 64-bit version of ','
	pop %rsi			# get character in the 8 lowest bits of %si
	movq $FormatString3, %rdi	# "%c\n"
	movl	$0, %eax
	call	printf@PLT
	movq $0, %rax
	movb $' ',%al
	push %rax	# push a 64-bit version of ' '
	pop %rsi			# get character in the 8 lowest bits of %si
	movq $FormatString3, %rdi	# "%c\n"
	movl	$0, %eax
	call	printf@PLT
	movq $0, %rax
	movb $'b',%al
	push %rax	# push a 64-bit version of 'b'
	pop %rsi			# get character in the 8 lowest bits of %si
	movq $FormatString3, %rdi	# "%c\n"
	movl	$0, %eax
	call	printf@PLT
	movq $0, %rax
	movb $':',%al
	push %rax	# push a 64-bit version of ':'
	pop %rsi			# get character in the 8 lowest bits of %si
	movq $FormatString3, %rdi	# "%c\n"
	movl	$0, %eax
	call	printf@PLT
	push b
	pop %rsi	# The value to be displayed
	movq $FormatString1, %rdi	# "%llu\n"
	movl	$0, %eax
	call	printf@PLT
	movq %rbp, %rsp		# Restore the position of the stack's top
	ret			# Return from main function
