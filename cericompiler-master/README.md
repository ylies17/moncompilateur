// Program := [DeclarationPart] StatementPart
// DeclarationPart := "[" Letter {"," Letter} "]"
// StatementPart := Statement {";" Statement} "."
// Statement := AssignementStatement
// AssignementStatement := Letter "=" Expression

// Expression := SimpleExpression [RelationalOperator SimpleExpression]
// SimpleExpression := Term {AdditiveOperator Term}
// Term := Factor {MultiplicativeOperator Factor}
// Factor := Number | Letter | "(" Expression ")"| "!" Factor
// Number := Digit{Digit}

// AdditiveOperator := "+" | "-" | "||"
// MultiplicativeOperator := "*" | "/" | "%" | "&&"
// RelationalOperator := "==" | "!=" | "<" | ">" | "<=" | ">="  
// Digit := "0"|"1"|"2"|"3"|"4"|"5"|"6"|"7"|"8"|"9"
// Letter := "a"|...|"z"
	
//<case statement> ::= case <expression> of <case list element> {; <case list element> } end
//<case list element> ::= <case label list> : <statement> | <empty>
//<case label list> ::= <constant> {, <constant> } 
//<empty>::=

//<constant> ::= <unsigned number> | <sign> <unsigned number> | <constant identifier> | <sign> <constant identifier> | <string> 
//<constant identifier> ::= <identifier>
//<unsigned number> ::= Number | <unsigned real>
//<unsigned real> ::= Number . Number | Number . Number E <scale factor> | <unsigned integer> E <scale factor>
//<scale factor> ::= Number | "+" Number | "-" Number
//<string> ::= '<character> {<character>}'
//<character> ::= <letter> | <digit>
//<sign> ::= "+" | "-"





Les fonctions que j'ai produit sont : 
ForStatement, Identifier_string ,case statement, case list element, case label list, empty, constant, constant identifier, unsigned number, unsigned real, scale factor, string, character et sign;

Toutes les fonctions se compilent bien d'après mes test malheuresement le case ne marche qu'avec des nombres car je n'ai pas réussi à comprendre comment et qu'elles instructions assembleur à mettre pour ces fonctions :  empty, constant, constant identifier, unsigned number, unsigned real, scale factor, string, character et sign;


Voici le lien de mon framagit : https://framagit.org/ylies17/moncompilateur
